$(document).ready(function ()
{
    $.post("listado.php", function (data)
    {
        $("#containers").html(data);
        $('#listado').dataTable({});
    });
	
    $(document).delegate("#listado tr", "click", function ()
    {
        $("#dinamics").html("");
        $('#listado tr').removeClass('selected');
        var table = $('#listado').DataTable();
        var aPos = $('#listado').dataTable().fnGetPosition(this);
        var aData = $('#listado').dataTable().fnGetData(aPos);
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
});