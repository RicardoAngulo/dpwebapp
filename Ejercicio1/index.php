<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Project/PHP/PHPProject.php to edit this template
-->
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="../css/button.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="../css/style.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="../css/DivValidation.css"/>
        <link rel="stylesheet" type="text/css" href="../css/input.css"/>
        <link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css"/>
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="../js/jquery.dataTables.js" ></script>
    </head>
    <body>
        <form method="post">
            <div class="wrapper">
                <input type="submit" class="myButtonBlue" name="consumirServicio" value="Consumir Servicio" />
            </div>
            <table id="ejercicio1" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th align="center">ID</th>
                        <th align="center">Tipo</th>
                        <th align="center">Color</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($_POST['consumirServicio'])) {
                        $jsonData = file_get_contents('https://my-json-server.typicode.com/dp-danielortiz/dptest_jsonplaceholder/items?color=green');
                        //imprime json en una tabla
                        foreach (json_decode($jsonData) as $key) {
                            echo "<tr class=''>";
                            echo '<td align="center">' . $key->id . '</td>';
                            echo '<td align="center">' . $key->type . '</td>';
                            echo '<td align="center">' . $key->color . '</td>';
                            echo "</tr>";
                            //imprime json por propiedad en consola del navegador con javascript
                            echo "<script>console.log('Console: ID: " . $key->id . " => Type: " . $key->type . " => Color: " . $key->color . "' );</script>";
                        }
                        //imprime json en consola del navegador con javascript
                        echo '<script type="text/javascript">';
                        echo 'console.log(' . json_encode($jsonData) . ')';
                        echo '</script>';

                        // exportamos archivo json
                        file_put_contents('Respuesta1.json', $jsonData);
                    }
                    ?>
                </tbody>
            </table>
        </form>
    </body>
</html>
