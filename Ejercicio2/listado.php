<table id="listado" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th align="center">ID</th>
            <th align="center">Tipo</th>
            <th align="center">Color</th>
        </tr>
    </thead>
    <tbody>		
    <script type="text/javascript">
        $.ajax({
            type: "GET",
            url: "https://my-json-server.typicode.com/dp-danielortiz/dptest_jsonplaceholder/items?color=red",
            dataType: "json",
            success: function (data)
            {
                var fila = '';
                for (var i = 0; i < JSON.parse(JSON.stringify(data)).length; i++)
                {
                    fila += "<tr>" +
                            "<td align='center'>" + JSON.parse(JSON.stringify(data[i].id)) + "</td>" +
                            "<td align='center'>" + JSON.parse(JSON.stringify(data[i].type)) + "</td>" +
                            "<td align='center'>" + JSON.parse(JSON.stringify(data[i].color)) + "</td>" +
                            "</tr>";
                }
                $('#listado').DataTable().destroy();
                $("#listado tbody").append(fila);
                $('#listado').DataTable().draw();
            }
        });
    </script>
</tbody>
<tfoot>
    <tr>
        <th align="center">ID</th>
        <th align="center">Tipo</th>
        <th align="center">Color</th>
    </tr>
</tfoot>
</table>