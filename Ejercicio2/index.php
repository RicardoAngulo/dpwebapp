<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Project/PHP/PHPProject.php to edit this template
-->
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="../css/button.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="../css/style.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="../css/DivValidation.css"/>
        <link rel="stylesheet" type="text/css" href="../css/input.css"/>
        <link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css"/>
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/scripts.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="../js/jquery.dataTables.js" ></script>
    </head>
    <body>
        <form action="" method="post" name="ejercicio2" class="ejercicio2">
            <div>
                <div id="header-wrapper">
                    <div id="header">
                        <div>Ejercicio2</div>
                        <div id="search">
                        </div>
                    </div>
                </div>
                <div id="page"><div class="inner_copy"></div>
                    <div id="page-bgtop">
                        <div id="content">
                            <div id="capa"></div>
                        </div>
                        <div id="update">
                            <div id="containers" align="center"></div>
                            <div id="dinamics" align="center"></div>
                        </div>
                        <div style="clear:both">&nbsp;</div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>