# DP Web App

Aplicación web en PHP para consumir servicio REST.

##     ⚡️Pre requisitos ##
1. Contar con la ultima version de [Java](https://www.java.com/es/download/) y el entorno de desarrollo [Apache NetBeans IDE 14](https://netbeans.apache.org/download/nb14/nb14.html)
2. contar con [XAMPP](https://www.apachefriends.org/es/index.html) ya que con esto nos da un servidor local

##     Para comenzar ##
1. Clonar este repositorio desde el CMD o git bash posicionandote en cualquier parte del equipo: https://gitlab.com/RicardoAngulo/dpwebapp.git
2. Iniciar el proyecto
3. Iniciar la depuración o ejecución del proyecto.

## Correr la aplicación

Para correr la aplicación solo necesitas desplegar la carpeta dpwebapp en la ruta C:\xampp\htdocs\

## Ejercicio 1 
Este ejercicio muestra la Respuesta Formateada por Consola y la exporta a un archivo json
```
ruta : http://localhost:8081/dpwebapp/Ejercicio1/index.php
```

## Ejercicio 2
Este ejercicio imprime el objeto json en una página HTML con Estilos DataTable
```
ruta : http://localhost:8081/dpwebapp/Ejercicio2/index.php
```

NOTA: En mi equipo local, cambie el puerto 80 por 8081, ya que el puerto 80 lo tengo ocupado con IIS.

## Dudas y comentarios les dejo mi correo:

ricardoangulo_@hotmail.com
